<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

//    /**
//     * @Route("/login", name="loginpage")
//     */
//    public function loginAction(Request $request)
//    {
//        // replace this example code with whatever you need
//        return $this->render('default/login.html.twig', [
//            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
//        ]);
//    }
    /**
     * @Route("/secure")
     */
    public function secureAction()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        return new Response('It works!');
    }
}
