<?php

namespace AppBundle\Controller;

use AppBundle\Dao\UserDao;
use AppBundle\Entity\WebserviceUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    private $userDao;
    /**
     * @Route("/login/{name}", name="loginpage")
     */
    public function loginAction($name)
    {
        $loginService = $this->get('login_service');

        return $this->render('auth/login.html.twig', [
            'name' => $loginService->getName(),
        ]);
    }
    public function getUserDao()
    {
        if (is_null($this->userDao)) {
            $this->userDao = new UserDao();
        }
        return $this->userDao;
    }
    /**
     * @Route("/load/{name}", name="loadpage")
     */
    public function load($name)
    {
        $annaAdmin = new WebserviceUser();
        $user = $this->getUserDao()->getUserById(1);
        $annaAdmin->setUser($user);
        $annaAdmin->setUsername('anna_admin');
        $annaAdmin->setEmail('anna_admin@example.com');
        $encoded = $this->container->get('security.password_encoder')
            ->encodePassword($annaAdmin, 'vithu');
        $annaAdmin->setPassword($encoded);

        $em = $this->getDoctrine()->getManager();
        $em->persist($annaAdmin);
        $em->flush();
    }
}
