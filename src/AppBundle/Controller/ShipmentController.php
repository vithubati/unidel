<?php
/**
 * Created by PhpStorm.
 * User: vithu
 * Date: 10/22/16
 * Time: 2:51 PM
 */

namespace AppBundle\Controller;
use AppBundle\Service\ShipmentService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/shipment")
 */
class ShipmentController extends Controller
{
    private $shipmentService;

    /**
     * @return ShipmentService
     */
    public function getShipmentService()
    {
        if (!($this->shipmentService instanceof ShipmentService)) {
            $this->shipmentService = new ShipmentService();
        }
        return $this->shipmentService;
    }

    /**
     * @Route("/shipmentbysender/{senderId}", name="shipmentAction")
     */
    public function getShipmentDetailsBySenderAction($senderId)
    {
        $shipmentList = $this->getShipmentService()->getShipmentDetailsBySender($senderId);
        $shipmentBundle =array();
        $dataBundle = array();
        if(count($shipmentList) >0){
            foreach ($shipmentList as $shipment) {
                $delivererName = null;
                $delivererId = null;
                if ($shipment->getDelivererId() != null) {
                    $delivererName = $shipment->getDelivererId()->getFirstName() . " " . $shipment->getDelivererId()->getLastName();
                    $delivererId = $shipment->getDelivererId()->getId();
                }
                $shipmentBundle[] = array(
                    "shipmentId" => $shipment->getShipId(),
                    "shipmentStatus" => $shipment->getShipmentStatus()->getName(),
                    "item" => $shipment->getItem(),
                    "pickupLocation" => $shipment->getPickupLocation()->getDistrict(),
                    "dropLocation" => $shipment->getDropLocation()->getDistrict(),
                    "delivererId" => $delivererId,
                    "delivererFullName" => $delivererName,
                    "cost" => $shipment->getCost(),
                    "timeLimit" => $shipment->getTimeLimit()->format('H:i:s A'),
                    "packageType" => $shipment->getPackageType(),
                    "packageWeight" => $shipment->getPackageWeight(),
                    "packageValue" => $shipment->getPackageValue(),
                    "enteredDate" => $shipment->getEnteredDate()->format('Y-m-d H:i:s A')
                );
            }
//            var_dump($shipmentBundle);die;
        }
        $dataBundle['shipment'] = $shipmentBundle;
        
        $response = new Response();
        $response->setContent(json_encode($dataBundle));
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');
//        $response->send();
        return $response;
    }

    /**
     * @Route("/shipmentbystatus/{senderId}/{statusId}", name="shipmentStatusAction")
     */
    public function getShipmentDetailsByStatusAction($senderId, $statusId)
    {
        $shipmentList = $this->getShipmentService()->getShipmentDetailsByStatus($senderId, $statusId);
        $shipmentBundle =array();
        $dataBundle = array();
        if(count($shipmentList) >0){
            foreach ($shipmentList as $shipment) {
                $delivererName = null;
                if ($shipment->getDelivererId() != null) {
                    $delivererName = $shipment->getDelivererId()->getFirstName() . " " . $shipment->getDelivererId()->getLastName();
                    $delivererId = $shipment->getDelivererId()->getId();
                }
                $shipmentBundle[] = array(
                    "shipmentStatus" => $shipment->getShipmentStatus()->getName(),
                    "item" => $shipment->getItem(),
                    "pickupLocation" => $shipment->getPickupLocation()->getDistrict(),
                    "dropLocation" => $shipment->getDropLocation()->getDistrict(),
                    "delivererId" => $delivererId,
                    "delivererFullName" => $delivererName,
                    "cost" => $shipment->getCost(),
                    "timeLimit" => $shipment->getTimeLimit()->format('H:i:s A'),
                    "packageType" => $shipment->getPackageType(),
                    "packageWeight" => $shipment->getPackageWeight(),
                    "packageValue" => $shipment->getPackageValue(),
                    "enteredDate" => $shipment->getEnteredDate()->format('Y-m-d H:i:s A')
                );
            }
//            var_dump($shipmentBundle);die;
        }
        $dataBundle['shipment'] = $shipmentBundle;

        $response = new Response();
        $response->setContent(json_encode($dataBundle));
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');
//        $response->send();
        return $response;
    }

    /**
     * @Route("/shipmentbydeliverer/{delivererId}", name="shipmentDelivererAction")
     */
    public function getShipmentDetailsByDelivererAction($delivererId)
    {
        $shipmentList = $this->getShipmentService()->getShipmentDetailsByDeliverer($delivererId);
        $shipmentBundle =array();
        $dataBundle = array();

        if(count($shipmentList) >0){
            foreach ($shipmentList as $shipment) {
                $delivererName = null;
                if ($shipment->getDelivererId() != null) {
                    $delivererName = $shipment->getDelivererId()->getFirstName() . " " . $shipment->getDelivererId()->getLastName();
                    $delivererId = $shipment->getDelivererId()->getId();
                }
                $shipmentBundle[] = array(
                    "shipmentStatus" => $shipment->getShipmentStatus()->getName(),
                    "item" => $shipment->getItem(),
                    "pickupLocation" => $shipment->getPickupLocation()->getDistrict(),
                    "dropLocation" => $shipment->getDropLocation()->getDistrict(),
                    "delivererId" => $delivererId,
                    "delivererFullName" => $delivererName,
                    "cost" => $shipment->getCost(),
                    "timeLimit" => $shipment->getTimeLimit()->format('H:i:s A'),
                    "packageType" => $shipment->getPackageType(),
                    "packageWeight" => $shipment->getPackageWeight(),
                    "packageValue" => $shipment->getPackageValue(),
                    "enteredDate" => $shipment->getEnteredDate()->format('Y-m-d H:i:s A')
                );
            }
//            var_dump($shipmentBundle);die;
        }
        $dataBundle['shipment'] = $shipmentBundle;

        $response = new Response();
        $response->setContent(json_encode($dataBundle));
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');
//        $response->send();
        return $response;
    }

    /**
     * @Route("/makeshipment", name="MakeShipment")
     * @Method({"POST"})
     */
    public function makeShipment(Request $request){
        if ($request->isMethod('POST')) {
//            if ($request->request->has('variable')) {
//            cool
//            }

            $savedShipment = $this->getShipmentService()->saveShipmentDetails($request->request->all());
//            var_dump($shipmentList);die;
            $result = array("result"=> $savedShipment);
            $response = new Response();
            $response->setContent(json_encode($result));
            $response->setStatusCode(Response::HTTP_OK);
            $response->headers->set('Content-Type', 'application/json');
            return $response;


        }
    }
}

