<?php
/**
 * Created by PhpStorm.
 * User: vithu
 * Date: 10/22/16
 * Time: 5:27 PM
 */

namespace AppBundle\Service;

use AppBundle\Dao\AddressDao;
use AppBundle\Dao\ShipmentDao;
use AppBundle\Dao\UserDao;
use AppBundle\Entity\Address;
use AppBundle\Entity\Shipment;
use AppBundle\Entity\User;
use AppBundle\Service;
class ShipmentService
{
    private $shipmentDao;
    private $addressDao;
    private $userDao;
    /**
     * @return ShipmentDao
     */
    public function getShipmentDao()
    {
        if (is_null($this->shipmentDao)) {
            $this->shipmentDao = new ShipmentDao();
        }
        return $this->shipmentDao;
    }

    /**
     * @return AddressDao
     */
    public function getAddressDao()
    {
        if (is_null($this->addressDao)) {
            $this->addressDao = new AddressDao();
        }
        return $this->addressDao;
    }

    /**
     * @return UserDao
     */
    public function getUserDao()
    {
        if (is_null($this->userDao)) {
            $this->userDao = new UserDao();
        }
        return $this->userDao;
    }
    /**
     * @param $senderId
     * @return \AppBundle\Entity\Shipment
     */
    public function getShipmentDetailsBySender($senderId)
    {
        return $this->getShipmentDao()->getShipmentDetailsBySender($senderId);
    }

    /**
     * @param $senderId
     * @param $statusId
     * @return \AppBundle\Entity\Shipment
     */
    public function getShipmentDetailsByStatus($senderId ,$statusId)
    {
        return $this->getShipmentDao()->getShipmentDetailsByStatus($senderId ,$statusId);
    }

    public function getShipmentDetailsByDeliverer($delivererId)
    {
        return $this->getShipmentDao()->getShipmentDetailsByDeliverer($delivererId);
    }

    public function saveShipmentDetails($shipmentData)
    {
//        var_dump($shipmentData['item']);die;
        $shipment = new Shipment();
        $user = $this->getUserDao()->getUserById($shipmentData['senderId']);
        $shipment->setSenderId($user);
        $status = $this->getShipmentDao()->getStatusById($shipmentData['shipmentStatus']);
        $shipment->setShipmentStatus($status);

        if($shipmentData['pickupLocation'] != null){
            $addressEm = $this->savePickupAddress($shipmentData);
            if($addressEm != null){
                $shipment->setPickupLocation($addressEm);
            }
        }else{
            $pickupAdd = $this->getAddressDao()->getAddressById($shipmentData['pickupLocationId']);
            $shipment->setPickupLocation($pickupAdd);
        }
        if($shipmentData['dropLocation'] != null){
            $addressEm = $this->saveDropAddress($shipmentData);
            if($addressEm != null){
                $shipment->setDropLocation($addressEm);
            }
        }else{
            $dropAdd = $this->getAddressDao()->getAddressById($shipmentData['dropLocationId']);
            $shipment->setDropLocation($dropAdd);
        }
        $shipment->setItem($shipmentData['item']);
//        $shipment->setDelivererId($shipmentData['delivererId']);
        $shipment->setTimeLimit(new \DateTime($shipmentData['timeLimit']));
        $shipment->setCost($shipmentData['cost']);
        $shipment->setPackageType($shipmentData['packageType']);
        $shipment->setPackageWeight($shipmentData['packageWeight']);
        $shipment->setPackageValue($shipmentData['packageValue']);
        $shipment->setEnteredDate(new \DateTime("now"));
        return $this->getShipmentDao()->saveShipmentDetails($shipment);
    }

    /**
     * @param $adddressData
     * @return mixed
     */
    private function savePickupAddress($adddressData){
        $address = new Address();
        $address->setAddress1($adddressData['addresss1P']);
        $address->setAddress2($adddressData['address2P']);
        $address->setCity($adddressData['cityP']);
        $address->setDistrict($adddressData['districtP']);
        $address->setProvince($adddressData['provinceP']);
        $address->setCountry('Sri Lanka');
        $savedId = $this->getAddressDao()->saveAddressDetails($address);

        return $savedId;
    }

    /**
     * @param $adddressData
     * @return mixed
     */
    private function saveDropAddress($adddressData){
        $address = new Address();
        $address->setAddress1($adddressData['addresss1D']);
        $address->setAddress2($adddressData['address2D']);
        $address->setCity($adddressData['cityD']);
        $address->setDistrict($adddressData['districtD']);
        $address->setProvince($adddressData['provinceD']);
        $address->setCountry('Sri Lanka');
        $savedId = $this->getAddressDao()->saveAddressDetails($address);

        return $savedId;
    }
}