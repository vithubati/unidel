<?php
/**
 * Created by PhpStorm.
 * User: bati
 * Date: 12/4/16
 * Time: 10:22 AM
 */

namespace AppBundle\Security;


use AppBundle\Entity\WebserviceUser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class WebserviceUserProvider implements UserProviderInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        $user = $this->em->getRepository('AppBundle:WebserviceUser')
            ->findOneBy(['username' => $username]);
        if(!$user){
            throw new UsernameNotFoundException();
        }
        return $user;
    }

    /**
     * Loads the user for the given token.
     *
     * This method must throw TokenNotFoundException if the user is not
     * found.
     *
     * @param string $token The token
     *
     * @return UserInterface
     *
     * @throws TokenNotFoundException if the user is not found
     */
    public function loadUserByApiToken($token)
    {
        $user = $this->em->getRepository('AppBundle:WebserviceUser')
            ->findOneBy(['apiToken' => $token]);
        if(!$user){
            throw new TokenNotFoundException();
        }
        return $user;
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        if (!$user instanceof WebserviceUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === 'AppBundle\Entity\WebserviceUser';
    }
}