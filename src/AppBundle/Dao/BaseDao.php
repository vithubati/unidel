<?php
/**
 * Created by PhpStorm.
 * User: vithu
 * Date: 11/4/16
 * Time: 11:27 AM
 */

namespace AppBundle\Dao;


class BaseDao
{
    protected function getEntityManager()
    {
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        return $em;
    }
}