<?php

/**
 * Created by PhpStorm.
 * User: vithu
 * Date: 10/22/16
 * Time: 5:27 PM
 */

namespace AppBundle\Dao;

use AppBundle\Entity\Shipment;
class ShipmentDao extends BaseDao
{
    /**
     * @param $senderId
     * @return Shipment
     */
    public function getShipmentDetailsBySender($senderId)
    {
        $shipment = null;
        try {
            $shipment = new Shipment();
            $shipment = $this->getEntityManager()
                ->getRepository('AppBundle:Shipment')
                ->findBySenderId($senderId);

            return $shipment;
        } catch (Exception $e) {
        }
    }

    /**
     * @param $senderId
     * @param $statusId
     * @return Shipment
     */
    public function getShipmentDetailsByStatus($senderId, $statusId)
    {
        $shipment = null;
        try {
            $shipment = new Shipment();
            $shipment = $this->getEntityManager()
                ->getRepository('AppBundle:Shipment')
                ->findBy(
                    array('shipmentStatus' => $statusId, 'senderId' => $senderId),
                    array('shipId' => 'ASC')
                );
            return $shipment;
        } catch (Exception $e) {
            return $shipment;
        }
    }

    /**
     * @param $delivererId
     * @return Shipment
     */
    public function getShipmentDetailsByDeliverer($delivererId)
    {
        $shipment = null;
        try {
            $shipment = new Shipment();
            $shipment = $this->getEntityManager()
                ->getRepository('AppBundle:Shipment')
                ->findByDelivererId($delivererId);

            return $shipment;
        } catch (Exception $e) {
            return $shipment;
        }
    }

    /**
     * @param $shipment
     * @return mixed
     */
    public function saveShipmentDetails($shipment)
    {
        try {
            $shipmentEm = $this->getEntityManager();
            $shipmentEm->persist($shipment);
            $shipmentEm->flush();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $statusId
     * @return null
     */
    public function getStatusById($statusId)
    {
        $status = null;
        try {
            $status = $this->getEntityManager()
                ->getRepository('AppBundle:ShipmentStatus')
                ->find($statusId);

            return $status;
        } catch (Exception $e) {
        }
    }



}