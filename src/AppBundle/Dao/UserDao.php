<?php
/**
 * Created by PhpStorm.
 * User: vithu
 * Date: 11/11/16
 * Time: 4:25 PM
 */

namespace AppBundle\Dao;

use AppBundle\Entity\User;
class UserDao extends BaseDao
{
    /**
     * @param $userId
     * @return null
     */
    public function getUserById($userId)
    {
        $user = null;
        try {
            $user = $this->getEntityManager()
                ->getRepository('AppBundle:User')
                ->find($userId);

            return $user;
        } catch (Exception $e) {
        }
    }
}