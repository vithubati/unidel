<?php
/**
 * Created by PhpStorm.
 * User: vithu
 * Date: 11/11/16
 * Time: 4:22 PM
 */

namespace AppBundle\Dao;

use AppBundle\Entity\Address;
class AddressDao extends BaseDao
{
    /**
     * @param $addressId
     * @return null
     */
    public function getAddressById($addressId)
    {
        $address = null;
        try {
            $address = $this->getEntityManager()
                ->getRepository('AppBundle:Address')
                ->find($addressId);

            return $address;
        } catch (Exception $e) {
        }
    }

    /**
     * @param $address
     * @return mixed
     */
    public function saveAddressDetails($address)
    {   $savedId = 0;
        try {
            $addressEm = $this->getEntityManager();
            $addressEm->persist($address);
            $addressEm->flush();

            return $address;
        } catch (Exception $e) {
            return $address;
        }
    }
}