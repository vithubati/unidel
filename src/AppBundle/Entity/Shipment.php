<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shipment
 */
class Shipment
{
    /**
     * @var integer
     */
    private $shipId;

    /**
     * @var string
     */
    private $item;

    /**
     * @var \DateTime
     */
    private $timeLimit;

    /**
     * @var float
     */
    private $cost;

    /**
     * @var string
     */
    private $packageType;

    /**
     * @var float
     */
    private $packageWeight;

    /**
     * @var float
     */
    private $packageValue;

    /**
     * @var \DateTime
     */
    private $enteredDate;

    /**
     * @var \AppBundle\Entity\ShipmentStatus
     */
    private $shipmentStatus;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $pickupLocation;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $dropLocation;

    /**
     * @var \AppBundle\Entity\User
     */
    private $senderId;

    /**
     * @var \AppBundle\Entity\User
     */
    private $delivererId;


    /**
     * Get shipId
     *
     * @return integer 
     */
    public function getShipId()
    {
        return $this->shipId;
    }

    /**
     * Set item
     *
     * @param string $item
     * @return Shipment
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return string 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set timeLimit
     *
     * @param \DateTime $timeLimit
     * @return Shipment
     */
    public function setTimeLimit($timeLimit)
    {
        $this->timeLimit = $timeLimit;

        return $this;
    }

    /**
     * Get timeLimit
     *
     * @return \DateTime 
     */
    public function getTimeLimit()
    {
        return $this->timeLimit;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return Shipment
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set packageType
     *
     * @param string $packageType
     * @return Shipment
     */
    public function setPackageType($packageType)
    {
        $this->packageType = $packageType;

        return $this;
    }

    /**
     * Get packageType
     *
     * @return string 
     */
    public function getPackageType()
    {
        return $this->packageType;
    }

    /**
     * Set packageWeight
     *
     * @param float $packageWeight
     * @return Shipment
     */
    public function setPackageWeight($packageWeight)
    {
        $this->packageWeight = $packageWeight;

        return $this;
    }

    /**
     * Get packageWeight
     *
     * @return float 
     */
    public function getPackageWeight()
    {
        return $this->packageWeight;
    }

    /**
     * Set packageValue
     *
     * @param float $packageValue
     * @return Shipment
     */
    public function setPackageValue($packageValue)
    {
        $this->packageValue = $packageValue;

        return $this;
    }

    /**
     * Get packageValue
     *
     * @return float 
     */
    public function getPackageValue()
    {
        return $this->packageValue;
    }

    /**
     * Set enteredDate
     *
     * @param \DateTime $enteredDate
     * @return Shipment
     */
    public function setEnteredDate($enteredDate)
    {
        $this->enteredDate = $enteredDate;

        return $this;
    }

    /**
     * Get enteredDate
     *
     * @return \DateTime 
     */
    public function getEnteredDate()
    {
        return $this->enteredDate;
    }

    /**
     * Set shipmentStatus
     *
     * @param \AppBundle\Entity\ShipmentStatus $shipmentStatus
     * @return Shipment
     */
    public function setShipmentStatus(\AppBundle\Entity\ShipmentStatus $shipmentStatus = null)
    {
        $this->shipmentStatus = $shipmentStatus;

        return $this;
    }

    /**
     * Get shipmentStatus
     *
     * @return \AppBundle\Entity\ShipmentStatus 
     */
    public function getShipmentStatus()
    {
        return $this->shipmentStatus;
    }

    /**
     * Set pickupLocation
     *
     * @param \AppBundle\Entity\Address $pickupLocation
     * @return Shipment
     */
    public function setPickupLocation(\AppBundle\Entity\Address $pickupLocation = null)
    {
        $this->pickupLocation = $pickupLocation;

        return $this;
    }

    /**
     * Get pickupLocation
     *
     * @return \AppBundle\Entity\Address 
     */
    public function getPickupLocation()
    {
        return $this->pickupLocation;
    }

    /**
     * Set dropLocation
     *
     * @param \AppBundle\Entity\Address $dropLocation
     * @return Shipment
     */
    public function setDropLocation(\AppBundle\Entity\Address $dropLocation = null)
    {
        $this->dropLocation = $dropLocation;

        return $this;
    }

    /**
     * Get dropLocation
     *
     * @return \AppBundle\Entity\Address 
     */
    public function getDropLocation()
    {
        return $this->dropLocation;
    }

    /**
     * Set senderId
     *
     * @param \AppBundle\Entity\User $senderId
     * @return Shipment
     */
    public function setSenderId(\AppBundle\Entity\User $senderId = null)
    {
        $this->senderId = $senderId;

        return $this;
    }

    /**
     * Get senderId
     *
     * @return \AppBundle\Entity\User 
     */
    public function getSenderId()
    {
        return $this->senderId;
    }

    /**
     * Set delivererId
     *
     * @param \AppBundle\Entity\User $delivererId
     * @return Shipment
     */
    public function setDelivererId(\AppBundle\Entity\User $delivererId = null)
    {
        $this->delivererId = $delivererId;

        return $this;
    }

    /**
     * Get delivererId
     *
     * @return \AppBundle\Entity\User 
     */
    public function getDelivererId()
    {
        return $this->delivererId;
    }
}
